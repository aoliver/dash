/* jslint params */

/*jslint
    this, browser
*/

/*global
    window, class
*/

(function () {
    "use strict";

    //each - helper function
    const each = function (nodex, callback) {
        if (typeof nodex === "object" && nodex.length > 0 && typeof callback === "function") {
            Object.keys(nodex).forEach(function (index) {
                callback.call(index, nodex[index]);
            });
        } else if (typeof nodex === "object" && nodex.nodeName && typeof callback === "function") {
            callback.call(this, nodex);
        }
    };

    //get nodes helper
    const get = function (the_query) {
        const the_nodes = document.querySelectorAll(the_query);
        return (the_nodes.length === 1
            ? the_nodes[0]
            : the_nodes);
    };

    //bind events helper
    const bind = function (this_node, events_object) {
        if (typeof this_node === "object" && typeof events_object === "object") {
            Object.keys(events_object).forEach(function (objext_index) {
                if (typeof events_object[objext_index] === "function") {
                    this_node.addEventListener(objext_index, events_object[objext_index]);
                }
            });
        }
    };

    //hxr post helper
    /*
    const xhr_post = function (data_object) {
        if (typeof data_object === "object" && data_object.data && typeof data_object.data === "object") {
            const base_64_data = window.btoa(JSON.stringify(data_object.data));
            const xhr = new XMLHttpRequest();
            xhr.open("POST", (data_object.url || "/"), true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            xhr.onload = data_object.load || function (data) {
                console.log(data.target.response);
            };

            xhr.send("data=" + base_64_data);
        }
    };
    */

    //actions list
    window.dash_actions = {
        logout: function (e) {
            console.log(e);
        }
    };

    //mobile nav
    const burder = document.querySelector("#burger");
    if (burder) {
        burder.onclick = function () {
            document.body.classList.toggle("show_mobile_nav");
        };
    }

    //show section
    const show_section = function (section_id) {

        //get section objects
        const selected_section = get("[data-section-id=\"" + section_id + "\"]");

        //is array of objects
        if (selected_section.length && selected_section.length > 0) {

            //inactivate section
            each(get("main>section.active, aside nav .active"), function (e) {
                e.classList.remove("active");
            });

            //active section
            each(selected_section, function (e) {
                e.classList.add("active");
            });
        }

    };

    //each nav button
    each(get("aside nav button"), function (this_section) {
        this_section.addEventListener("click", function (e) {
            show_section(e.target.getAttribute("data-section-id"));
        });
    });

    //actions
    get("body").addEventListener("click", function (e) {
        const has_action = e.target.getAttribute("data-action");
        if (has_action && typeof window.dash_actions[has_action] === "function") {
            window.dash_actions[has_action].call(this, e.target);
        }
    });

    //inputs
    each(get("input"), function (the_input) {
        bind(the_input, {
            focus: function (e) {
                e.target.parentNode.classList.add("focused");
            },
            blur: function (e) {
                if (e.target.value.length <= 0) {
                    e.target.parentNode.classList.remove("focused");
                }
            }
        });
    });

    //loaders
    each(get(".loader"), function (this_loader) {
        let i = 0;
        for (i = 1; i <= 4; i += 1) {
            this_loader.appendChild(document.createElement("div"));
        }
    });

    //form buttons
    each(get("button, .button"), function (this_button) {
        this_button.addEventListener("click", function (e) {
            e.target.classList.toggle("processing");
        });
    });
}());