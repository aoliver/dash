# Dash

Dash is a simple dashboard UI that can be modified and moulded easily.

The current build of dash is not build on PHP even though some files use PHP, this is just for testing on a server environment.

## Running Dash

To run Dash straight from clone or download

1. first jump into the terminal
2. cd into the dash core folder
3. run the below php command

```txt
php -S localhost:1400
```

If you have php installed on your system still create a development server from which Dash will run from.

Open a new browser window and navigate to http://localhost:1400 you should see dash.

### The HTML Way

Alternatively you can rename the .php files to .html and then open them in a browser. **Note:** You may need to re-link some assets.

## Compatibility

### Browsers

1. Chrome
2. Firefox
3. Safari
4. Edge

### Javascript

1. ES5