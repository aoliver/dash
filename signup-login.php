<!DOCTYPE html>
<html lang="en">
<head>
    <!-- char set -->
    <meta charset="UTF-8" />
    
    <!-- browser scale -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes" />

    <!-- title -->
    <title>Dash - Signup or Login</title>

    <!-- manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Dash PWA">
    <link rel="apple-touch-icon" href="/assets/icons/512.png">

    <!-- description -->
    <meta name="description" content="A sample dash app">

    <!-- theme color -->
    <meta name="theme-color" content="#404B47" />


    <!-- dash styles -->
    <link rel="stylesheet" type="text/css" href="/assets/css/globals.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/boilerplate.css">
</head>
<body>

    <!-- Sign up or Login dialog -->
    <link rel="stylesheet" type="text/css" href="/assets/css/signup-login.css">
    <main>

        <!-- login -->
        <div id="sections">
            
            <section>
                <div class="content">
                    <h1>Welcome</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>
            </section>

            <section>
                <div class="content">
                    <h2>Login</h2>

                    <form method="post">
                        <fieldset>

                            <p data-type="input">
                                <label for="email">Email:</label>
                                <input type="email" name="email" id="email" autocomplete="off" required>
                                <small>Example: example@example.com</small>
                            </p>

                            <p data-type="input">
                                <label for="password">Password:</label>
                                <input type="password" name="password" id="password" autocomplete="off" required>
                                <small>Password stringth</small>
                            </p>
                            

                        </fieldset>

                        <button type="button"><span>Login</span><span class="loader"></span></button>
                    </form>
                </div>
            </section>
        </div>

        <!-- sign up -->


        <!-- forgotten password -->


        <p id="footer-content">
            <span>Don't have an <a href="#newaccount">account?</a></span> <span class="spacer">/</span> <span>Forgot your <a href="#newpassword">password?</span></a>
        </p>
    </main>


    <!-- dash scripts -->
    <script type="text/javascript" src="/assets/js/dash.js" defer></script>

    <noscript>
        <p><strong>Oops!</strong> Looks like JavaScript it turned off.</p>
    </noscript>
</body>
</html>