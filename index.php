<!DOCTYPE html>
<html lang="en">
<head>
    <!-- char set -->
    <meta charset="UTF-8" />
    
    <!-- browser scale -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10, user-scalable=yes" />

    <!-- title -->
    <title>Dash - Main</title>

    <!-- manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Add iOS meta tags and icons -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Dash PWA">

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favs/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favs/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favs/favicon-16x16.png">

    <!-- description -->
    <meta name="description" content="A sample dash app">

    <!-- theme color -->
    <meta name="theme-color" content="#404B47" />

    <!-- google Font -->
    <link href="https://fonts.googleapis.com/css?family=Squada+One&display=swap" rel="stylesheet">

    <!-- dash globals -->
    <link rel="stylesheet" type="text/css" href="/assets/css/globals.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/boilerplate.css">
</head>
<body>

    <!-- sidebar -->
    <link rel="stylesheet" type="text/css" href="/assets/css/aside.css">
    <aside>

        <div id="top_banner">
            <!-- logo -->
            <div id="dash-logo"><i>Dash</i></div>

            <!-- burger nav icon -->
            <button id="burger"><span></span><span></span><span></span></button>
        </div>

        <!-- navigation -->
        <nav>
            <section class="has_avatar">

                <h4 class="heading">
                    <span class="avatar">&nbsp;</span>
                    Alex Oliver
                    <small>Administrator</small>
                </h4>

                <button data-section-id="n_messages">New messages <span class="count alert">10</span></button>
                <button data-section-id="a_settings">Settings</button>
                <button data-action="logout">Log out</button>
            </section>

            <section>
                <h4 class="heading">Orders</h4>
                <button data-section-id="n_orders">New Orders <span class="count">10</span></button>
                <button data-section-id="p_orders">Pending Orders <span class="count">200</span></button>
                <button data-section-id="c_orders">Complete Orders</button>
            </section>


            <section>
                <h4 class="heading">Sections</h4>
                <button class="active" data-section-id="sec1">Section 1</button>
                <button data-section-id="sec2">Section 2 <span class="count">4</span></button>
            </section>

        </nav>
    </aside>    



    <!-- main container -->
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <main>
        
        <!-- section 1 -->
        <section class="active" data-section-id="sec1" data-action="load_dash">
            <header>
                <h3>Section 1</h3>
            </header>
            <div class="content inner_content">
                <h1>Heading 1</h1>
                <h2>Heading 2</h2>
                <h3>Heading 3</h3>
                <h4>Heading 4</h4>

                <p><strong>Paragraph:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Euismod lacinia at quis risus sed. Gravida quis blandit turpis cursus in hac habitasse platea dictumst. Bibendum arcu vitae elementum curabitur. Magna etiam tempor orci eu lobortis elementum.</p>
            
                <p>
                    <button>Button</button>
                    <button class="go">Button Go</button>
                    <button class="alert">Button Alert</button>
                </p>

                <form method="post">
                    <fieldset>

                        <p data-type="input">
                            <label for="email">Email:</label>
                            <input type="email" name="email" id="email" autocomplete="off" required>
                            <small>Example: example@example.com</small>
                        </p>

                        <p data-type="input">
                            <label for="password">Password:</label>
                            <input type="password" name="password" id="password" autocomplete="off" required>
                            <small>Password strength</small>
                        </p>
                        
                    </fieldset>

                    <fieldset>
                        <h4 class="banner">Span 2 Column Form</h4>
                        <div class="span span2">
                            <p data-type="input">
                                <label for="fname">First name:</label>
                                <input type="text" name="fname" id="fname" autocomplete="off" required>
                                <small>Example: John</small>
                            </p>

                            <p data-type="input">
                                <label for="lname">Last name:</label>
                                <input type="text" name="lname" id="lname" autocomplete="off" required>
                                <small>Example: Snow</small>
                            </p>
                        </div>
                    </fieldset>

                    <fieldset>
                        <h4 class="banner">Span 3 Column Form</h4>

                        <div class="span span3">
                            <p data-type="input">
                                <label for="fname_2">First name:</label>
                                <input type="text" name="fname_2" id="fname_2" autocomplete="off" required>
                                <small>Example: John</small>
                            </p>

                            <p data-type="input">
                                <label for="lname_2">Last name:</label>
                                <input type="text" name="lname_2" id="lname_2" autocomplete="off" required>
                                <small>Example: Snow</small>
                            </p>

                            <p data-type="input">
                                <label for="phone">Phone Number</label>
                                <input type="tel" name="phone" id="phone" autocomplete="off" required>
                                <small>Example: 1234543256</small>
                            </p>
                        </div>
                    </fieldset>

                </form>

                <!-- table -->
                <h4 class="banner">Table</h4>

                
                <div class="table">
                    <div class="row header">
                        <span>header 1</span>
                        <span>header 2</span>
                        <span>header 3</span>
                    </div>
                    <div class="row">
                        <span>column 1</span>
                        <span>column 2</span>
                        <span>column 3</span>
                    </div>
                    <div class="row">
                        <span>column 4</span>
                        <span>column 5</span>
                        <span>column 6</span>
                    </div>    
                </div>


                <!-- loader -->
                <h4 class="banner">Loader</h4>
                <p>Loader: <span class="loader"></span></p>
                
                <p>
                    <button type="button"><span>Submit with loader</span><span class="loader"></span></button>
                </p>

            </div>
        </section>


        <!-- section 2 -->
        <section data-section-id="sec2" data-action="load_section_2">
            <header>
                <h3>Section 2</h3>
            </header>
            <div class="content has_nav">
                <nav>
                    <a href="#item_1" class="active has_dot">Item 1<small>1st Jan 1970 13:00:00</small></a>
                    <a href="#item_2" class="has_dot">Item 2</a>
                    <a href="#item_3" class="has_dot">Item 3</a>
                    <a href="#item_4" class="has_dot dot_inactive">Item 4</a>
                </nav>

                <div class="sections">
                    <section class="inner_content active">
                        <h1>Lorem ipsum dolor sit amet</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Sodales ut eu sem integer vitae justo eget magna. Nisi lacus sed viverra tellus in hac habitasse platea. Sagittis vitae et leo duis ut diam quam nulla porttitor.</p>
                    </section>

                    <section class="inner_content">
                        <h1>Lorem ipsum dolor sit amet - Section 2</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Sodales ut eu sem integer vitae justo eget magna. Nisi lacus sed viverra tellus in hac habitasse platea. Sagittis vitae et leo duis ut diam quam nulla porttitor.</p>
                    </section>
                </div>

            </div>
        </section>

    </main>


    <!-- dash scripts -->
    <script type="text/javascript" src="/assets/js/dash.js" defer></script>

    <noscript>
        <p>Darn... Looks like JavaScript it turned off.</p>
    </noscript>
</body>
</html>